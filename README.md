# Apache ZooKeeper Docker Image

[![pipeline status](https://gitlab.com/rychly-edu/docker/docker-zookeeper/badges/master/pipeline.svg)](https://gitlab.com/rychly-edu/docker/docker-zookeeper/commits/master)
[![coverage report](https://gitlab.com/rychly-edu/docker/docker-zookeeper/badges/master/coverage.svg)](https://gitlab.com/rychly-edu/docker/docker-zookeeper/commits/master)

The image is based on [ZooKeeper Docker repo](https://hub.docker.com/_/zookeeper/).
The version of the base image (its Docker tag) can be restricted on build by the `FROM_TAG` build argument.

## Build

### The Latest Version by Docker

~~~sh
docker build --pull -t "registry.gitlab.com/rychly-edu/docker/docker-zookeeper:latest" .
~~~

### All Versions by the Build Script

~~~sh
./build.sh --build "registry.gitlab.com/rychly-edu/docker/docker-zookeeper" "latest"
~~~

For the list of versions to build see [docker-tags.txt file](docker-tags.txt).

## Run by Docker-Compose

See [docker-compose.yml](docker-compose.yml).
