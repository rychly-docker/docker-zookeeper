ARG FROM_TAG
FROM zookeeper:${FROM_TAG:-latest}

MAINTAINER Marek Rychly <marek.rychly@gmail.com>

COPY scripts /

RUN true \
# make the scripts executable
&& chmod 755 /*.sh

ENTRYPOINT ["/entrypoint.sh"]

CMD ["zkServer.sh", "start-foreground"]

HEALTHCHECK CMD /healthcheck.sh
