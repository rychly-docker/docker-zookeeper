#!/bin/sh

exec su zookeeper -c 'zkCli.sh -server localhost:${ZOO_PORT} stat / || exit 1'
