#!/bin/sh
#
# fix ISSUE: os::commit_memory failed; error=Operation not permitted
# (AUFS does not support xattr, so we need to set the flag once again after execution of the container in its entrypoint)
# https://en.wikibooks.org/wiki/Grsecurity/Application-specific_Settings#Java
#

[[ ! -e /usr/bin/setfattr ]] && apk --no-cache add attr

setfattr -n user.pax.flags -v em /usr/lib/jvm/default-jvm/bin/java /usr/lib/jvm/default-jvm/jre/bin/java

exec /docker-entrypoint.sh ${@}
